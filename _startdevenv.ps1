# Import environment variables #####################################################################
. "${PSScriptRoot}/prj/setup.ps1"
if ($LastExitCode -ne 0) { exit 1 }
# Start development environment ####################################################################
Write-Host ">>> Opening VSCode"
code --new-window "`"${env:CRDNL_ROOT_DIR}`""