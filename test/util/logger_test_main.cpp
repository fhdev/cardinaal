#include <chrono>
#include <thread>
#include "assert.hpp"
#include "logger.hpp"

int LoggerTest(void);

int main(int argc, char *argv[]) {
    LoggerTest();

    return 0;
}

int LoggerTest(void) {
    using namespace crdnlutil;
    MainLogger(LogLevel::DEBUG, "crdnl.log");
    LOG(LogLevel::DEBUG,   "Debug level log message.");
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    LOG(LogLevel::INFO,    "Info level log message.");
    std::this_thread::sleep_for(std::chrono::milliseconds(200));
    LOG(LogLevel::WARNING, "Warning level log message.");
    std::this_thread::sleep_for(std::chrono::milliseconds(300));
    LOG(LogLevel::ERROR,   "Error level log message.");
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    LOG(LogLevel::INFO,    "First line of multiline info level log message.\n"
        "Second line of multiline info level log message.\n"
        "Third line of multiline info level log message.");
    std::this_thread::sleep_for(std::chrono::milliseconds(800));
    LOG_DEBUG("Conditional debug logger debug level log message.");
    return 0;
}