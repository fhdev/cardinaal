#pragma once
#ifndef CRDNL_ASSERT_H
#define CRDNL_ASSERT_H

#include <exception>
#include <iostream>

#ifndef NDEBUG

#define ASSERT(condition, message) \
    do { \
        if (! (condition)) { \
            std::cerr << "Assertion \"" #condition "\" failed in file \"" << __FILE__ \
                      << "\" function \"" << __FUNCTION__ << "\" line " << __LINE__ << \
                      ": " << (message) << std::endl; \
            std::abort(); \
        } \
    } while (false)

#define ASSERT_EQUAL(value1, value2) \
    do { \
        if ((value1) != (value2)) { \
            std::cerr << "Assertion failed in file \"" << __FILE__ << "\" function \"" \
                      << __FUNCTION__ << "\" line " << __LINE__ \
                      << ": \"" << #value1 "\" (" << value1 << ") is not equal to \"" #value2 "\" (" \
                      << value2 << ")" << std::endl; \
            std::abort(); \
        } \
    } while (false)

#define ASSERT_EQUAL_TOL(value1, value2, tol) \
    do { \
        if ((((value1) - (value2)) > tol) || (((value1) - (value2)) < -tol)) { \
            std::cerr << "Assertion failed in file \"" << __FILE__ << "\" function \"" \
                      << __FUNCTION__ << "\" line " << __LINE__ \
                      << ": \"" << #value1 "\" (" << value1 << ") is not equal to \"" #value2 "\" (" \
                      << value2 << ") within tolerance of \"" #tol "\" (" << tol << ")" << std::endl; \
            std::abort(); \
        } \
    } while (false)

#else

#define ASSERT(condition, message) do { } while (false)

#define ASSERT_EQUAL(value1, value2) do { } while (false)

#define ASSERT_EQUAL_TOL(value1, value2, tol) do { } while (false)

#endif


#endif // CRDNL_ASSERT_H