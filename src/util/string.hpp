#pragma once
#ifndef CRDNL_HELPER_H
#define CRDNL_HELPER_H

#include <string>

namespace crdnlutil {

template<typename T>
bool TryParse(const std::string& str, T& out) 
{
    using namespace std;
    istringstream in = istringstream(str);
    in >> out;
    return !in.fail() && in.eof();
}

std::string& ReplaceAll(std::string& str, const std::string& oldSub, const std::string& newSub);

}  // namespace crdnlutil

#endif // CRDNL_HELPER_H