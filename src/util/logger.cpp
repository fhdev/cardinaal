#include <chrono>
#include <sstream>
#include <time.h>

#include "logger.hpp"
#include "string.hpp"

using std::left;
using std::ostringstream;
using std::setfill;
using std::setw;
using std::string;

namespace crdnlutil {

const string Logger::levelStrings_[] = {"OFF", "ERROR", "WARNING", "INFO", "DEBUG"};

Logger::Logger(LogLevel level, const string &path) : 
    level_(level), path_(path), cout_(std::cout.rdbuf()), fout_(nullptr) {
    if ((level_ != LogLevel::OFF) && !path_.empty()) {
        file_.open(path);
        fout_.rdbuf(file_.rdbuf());
    }
}

Logger::~Logger() {
    if (file_.is_open()) {
        file_.close();
    }
}

void Logger::operator()(LogLevel level, const string& message) { 
    if ((level_ != LogLevel::OFF) && (level_ >= level)) {
        writeMessage(level, message);
    }
}

void Logger::operator()(LogLevel level, const string& message, const string& file, 
    const string& function, int line) {
    if ((level_ != LogLevel::OFF) && (level_ >= level)) {
        std::ostringstream messageInfoStream;
        messageInfoStream << message << "    (in file \"" << file << "\" function \"" << function
                          << "\" line " << line << ")";
        writeMessage(level, messageInfoStream.str());
    }
}

void Logger::writeMessage(LogLevel level, const string& message) { 
    string messageCopy = message;
    ostringstream lineStream;
    lineStream << "| " << getTimestamp() << " | " << left << setfill(' ') << setw(7) 
               << levelStrings_[static_cast<int>(level)]  << " | " 
               // handle multi-line messages
               << ReplaceAll(messageCopy, "\n", "\n| _                       | _       | ")
               << std::endl;
    cout_ << lineStream.str();
    if (file_.is_open()) {
        fout_ << lineStream.str();
    }
}

std::string Logger::getTimestamp() {
    using namespace std;
    using namespace std::chrono;
    const system_clock::time_point now = system_clock::now();
    const time_t timeCurrent = system_clock::to_time_t(now);
    const milliseconds timeCurrentMs = duration_cast<milliseconds>(now.time_since_epoch()) % 1000;
    tm timeLocal;
#if defined(CRDNL_LINUX)
    localtime_r(&timeCurrent, &timeLocal);
#elif defined(CRDNL_WINDOWS)
    localtime_s(&timeLocal, &timeCurrent);
#endif
    ostringstream stream;
    stream << std::put_time(&timeLocal, "%Y.%m.%d %H:%M:%S")
          << '.' << std::setfill('0') << std::setw(3) << timeCurrentMs.count();
    return stream.str();
}

Logger& DebugLogger(LogLevel level, const string& path) {
    static Logger logger(level, path);
    return logger;
}

Logger& MainLogger(LogLevel level, const string& path) { 
    static Logger logger(level, path);
    return logger;
}

}