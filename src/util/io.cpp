#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include "io.hpp"

using std::string;

namespace crdnlutil {

string ReadFile(const string& path) {
    using namespace std;
    ifstream fileStream(path);
    stringstream buffer;
    if (fileStream.fail()) {
        buffer << "File " << path << " does not exist.";
        throw std::invalid_argument(buffer.str());
    }
    buffer << fileStream.rdbuf();
    fileStream.close();
    return buffer.str();
}

void WriteFile(const std::string &path, const std::string &content) {
    using namespace std;
    ofstream fileStream(path);
    fileStream << content;
    fileStream.close();
}

}