#include <sstream>
#include "string.hpp"

namespace crdnlutil {

std::string& ReplaceAll(std::string& str, const std::string& oldSub, const std::string& newSub) {
    size_t pos = 0;
    while ((pos = str.find(oldSub, pos)) != std::string::npos) {
        str.replace(pos, oldSub.length(), newSub);
        pos += newSub.length();
    }
    return str;
}

}  // namespace crdnlutil