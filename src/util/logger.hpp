#pragma once
#ifndef CRDNL_LOGGER_H
#define CRDNL_LOGGER_H

#include <chrono>
#include <iomanip>
#include <iostream>
#include <string>
#include <fstream>

#ifdef CRDNL_DEBUG
#define LOG_DEBUG(msg) crdnlutil::DebugLogger()(crdnlutil::LogLevel::DEBUG, (msg), __FILE__, __FUNCTION__, __LINE__)
#else
#define LOG_DEBUG(_) do {} while(0)
#endif

#define LOG(level, msg) crdnlutil::MainLogger()((level), (msg))

namespace crdnlutil {

enum class LogLevel:uint64_t { OFF=0, ERROR=1, WARNING=2, INFO=3, DEBUG=4 };

class Logger {
   private:
    LogLevel level_;
    std::string path_;
    std::ofstream file_;
    std::ostream cout_;
    std::ostream fout_;

    static const std::string levelStrings_[];

    void writeMessage(LogLevel level, const std::string& message);

   public:
    Logger(LogLevel level = LogLevel::INFO, const std::string &path = "");

    Logger(const Logger& other) = delete;

    Logger(Logger&& other) = delete;

    Logger& operator= (const Logger&) = delete;

    ~Logger();

    void operator()(LogLevel level, const std::string& message);

    void operator()(LogLevel level, const std::string& message, const std::string& file, 
        const std::string& function, int line);

    static std::string getTimestamp();
};

Logger& DebugLogger(LogLevel level = LogLevel::DEBUG, const std::string& path = "");

Logger& MainLogger(LogLevel level = LogLevel::OFF, const std::string& path = "");


}  // namespace crdnlutil

#endif  // CRDNL_LOGGER_H