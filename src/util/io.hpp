#pragma once
#ifndef CRDNL_IO_H
#define CRDNL_IO_H

#include <string>

namespace crdnlutil {

std::string ReadFile(const std::string &path);

void WriteFile(const std::string &path, const std::string &content);

}

#endif // CRDNL_IO_H