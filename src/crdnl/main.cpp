#include <chrono>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <thread>
#include "logger.hpp"

int main(void) {
    using namespace std;
    using namespace crdnlutil;
    try {
        MainLogger(LogLevel::INFO, "crdnl.log");
        LOG(LogLevel::INFO, "Welcome from crdnl executable.");
        LOG_DEBUG("Welcome from crdnl executable.");
    } catch (const std::exception& e) {
        stringstream s;
        s << "Unexpected exception happened. " << e.what();
        LOG(LogLevel::ERROR, s.str());
        LOG_DEBUG(s.str());
    }
    return 0;
}