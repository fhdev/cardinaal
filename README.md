# CARDINAAL
CAR Dynamics Is Not At All Linear
## Development Guide
### Required Tools
The following tools are required for development:
- Python 3.10 shall be installed and the install location shall be added to `<crdnl_root>/prj/python_install_dirs.txt` .
- CMake 3.27 shall be installed and the install location shall be added to `<crdnl_root>/prj/cmake_install_dirs.txt` .
- GCC 13.2 shall be installed and the install location shall be added to `<crdnl_root>/prj/gcc_install_dirs.txt` .
- Visual Studio Code 1.84 shall be installed and added to PATH.
### Start the Development Environment
- The Development Environment can be started by running the script `<crdnl_root>/_startdevenv.sh` from bash shell or `<crdnl_root>/_startdevenv.ps1` from powershell.
- Warning: no other Visual Studio Code instances shall run before starting CAARDINAL development environment, because environment variables can not be set properly in this case.
### Naming convention
Entity                                  | Naming                                 | Notes
----------------------------------------|----------------------------------------|----------------------------------------
macro                                   | SOME_MACRO                             |
namespace                               | somenamespace                          |
enumeration value                       | SOME_ENUMERATION_VALUE                 |
global variable                         |                                        | There shall be no global variables.
function                                | SomeFunction                           |
function parameter                      | someParameter                          |
function local variable                 | someVariable                           |
class                                   | SomeClass                              |
private class variable                  | somePrivateVariable_                   |
public class  variable                  |                                        | There shall be no public class variables.
private class method                    | somePrivateMethod                      |
public  class method                    | SomePublicMethod                       |

