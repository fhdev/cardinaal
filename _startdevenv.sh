#!/bin/bash
# Import environment variables #####################################################################
SCRIPTROOT="$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &> /dev/null && pwd)"
. "${SCRIPTROOT}/prj/setup.sh"  || exit 1
# Start development environment ####################################################################
echo ">>> Opening VSCode"
code --new-window "${CRDNL_ROOT_DIR}" &
