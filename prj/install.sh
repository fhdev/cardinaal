#!/bin/bash
# Import environment variables #####################################################################
SCRIPTROOT="$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &> /dev/null && pwd)"
. "${SCRIPTROOT}/env.sh" || exit ${CRDNL_FAIL}
# Functions ########################################################################################
# Function to find python installation and create python virtual environment
function createPythonVirtualEnvironment() {
	# Create virtual environment
    if [ -d "${CRDNL_PYTHON_DIR}" ]; then
        echo ">>> Delete existing python virtual environment at '${CRDNL_PYTHON_DIR}'"
        rm -rf "${CRDNL_PYTHON_DIR}"
        mkdir "${CRDNL_PYTHON_DIR}"
    fi
    echo ">>> Creating python virtual environment in '${CRDNL_PYTHON_DIR}'"
    "${CRDNL_PYTHON_INSTALL_DIR}/python" -m venv "${CRDNL_PYTHON_DIR}"
	exitCode=$?
    if [ ${exitCode} -ne 0 ]; then
        echo ">>> ERROR: Unable to create python virtual environment. Exiting ..."
        exit ${CRDNL_FAIL}
    fi
	# Install packages inside virtual environment
	echo ">>> Install python packages in '${CRDNL_PYTHON_DIR}'"
	if [ ${CRDNL_OS} == ${CRDNL_OS_WINDOWS} ]; then
		pythonInterpreterPath="${CRDNL_PYTHON_DIR}/Scripts/python"
	else
		pythonInterpreterPath="${CRDNL_PYTHON_DIR}/python"
	fi
	"${pythonInterpreterPath}" "-m" "pip" "install" "--upgrade" "pip"
    "${pythonInterpreterPath}" "-m" "pip" "install" "-r" "${SCRIPTROOT}/python_requirements.txt"
	exitCode=$?
    if [ ${exitCode} -ne 0 ]; then
        echo ">>> ERROR: Unable to install python packages. Exiting ..."
        exit ${CRDNL_FAIL}
    fi
}
# Function to install arbitrary tar.gz file
function installTarGz() {
    targetName=${1}
    targetUrl=${2}
    targetArchive=${3}
    installDir=${4}
	# Delete existing installation
    if [ -d "${installDir}" ]; then
        echo ">>> Delete existing $targetName installation at '${installDir}'"
        rm -rf "${installDir}"
        mkdir "${installDir}"
    fi
	# Download tar.gz
    echo ">>> Downloading ${targetName} from '${targetUrl}'"
    wget -P "${installDir}" "${targetUrl}/${targetArchive}"
    exitCode=$?
    if [ ${exitCode} -ne 0 ]; then
        echo ">>> ERROR: Unable to download ${targetName}. Exiting ..."
        exit ${CRDNL_FAIL}
    fi
	# Extracting tar.gz
    echo ">>> Extracting ${targetName} to '${installDir}'"
    tar -zxf "${installDir}/${targetArchive}" -C "${installDir}"
    exitCode=$?
    if [ ${exitCode} -ne 0 ]; then
        echo ">>> ERROR: Unable to extract ${targetName}. Exiting ..."
        exit ${CRDNL_FAIL}
    fi
}
# Common variables #################################################################################
EXT_DIR="${ROOT_DIR}/ext"
# Install 3rd-party software #######################################################################
# Install boost
BOOST_NAME="boost"
BOOST_URL="https://boostorg.jfrog.io/artifactory/main/release/1.77.0/source"
BOOST_ARCHIVE="boost_1_77_0.tar.gz"
installTarGz "${BOOST_NAME}" "${BOOST_URL}" "${BOOST_ARCHIVE}" "${CRDNL_BOOST_DIR}"
# Install Eigen
EIGEN_NAME="Eigen"
EIGEN_URL="https://gitlab.com/libeigen/eigen/-/archive/3.4.0"
EIGEN_ARCHIVE="eigen-3.4.0.tar.gz"
installTarGz "${EIGEN_NAME}" "${EIGEN_URL}" "${EIGEN_ARCHIVE}" "${CRDNL_EIGEN_DIR}"
# Create Python virtual environment
createPythonVirtualEnvironment
