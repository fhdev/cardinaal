#!/bin/bash
# Ensure script is sourced rather than run #########################################################
if [ "${BASH_SOURCE[0]}" -ef "$0" ]; then
    echo "Please source this script instead of running it!"
    return 1
fi
# Get script root ##################################################################################
SCRIPTROOT="$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &> /dev/null && pwd)"
# Obtain operating system ##########################################################################
export CRDNL_OS_LINUX="linux"
export CRDNL_OS_WINDOWS="windows"
if [[ -z "${USERNAME}" ]]; then
  export CRDNL_OS="${CRDNL_OS_LINUX}"
else
  export CRDNL_OS="${CRDNL_OS_WINDOWS}"
fi
# Set up variables #################################################################################
export CRDNL_PASS=0
export CRDNL_FAIL=1
export CRDNL_TRUE=1
export CRDNL_FALSE=0
export CRDNL_ROOT_DIR="$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")/.." &> /dev/null && pwd)"
export CRDNL_BUILD_DIR="${CRDNL_ROOT_DIR}/build/$CRDNL_OS"
export CRDNL_BIN_DIR="${CRDNL_ROOT_DIR}/bin/$CRDNL_OS"
export CRDNL_EXT_DIR="${CRDNL_ROOT_DIR}/ext"
export CRDNL_BOOST_DIR="${CRDNL_EXT_DIR}/boost"
export CRDNL_EIGEN_DIR="${CRDNL_EXT_DIR}/eigen"
export CRDNL_PRJ_DIR="${CRDNL_ROOT_DIR}/prj"
export CRDNL_PYTHON_DIR="${CRDNL_EXT_DIR}/python/$CRDNL_OS"
# Find tools #######################################################################################
function findInstallDir() {
	swName="${1}"
	installDirsFile="${2}"
	envVarName="${3}"
	dirFound=${CRDNL_FALSE}
	installDir=""
	installDirs="$(grep -v '^#' < "${SCRIPTROOT}/${installDirsFile}")"
	while IFS= read -r line; do
		installDir="${line}"
		if [ -d "${installDir}" ]; then
			dirFound=${CRDNL_TRUE}
			break
		fi
	done <<< "${installDirs}"
	if [ ${dirFound} -eq ${CRDNL_TRUE} ]; then
		export "${envVarName}"="${installDir}"
		echo ">>> ${swName} installation found at '${installDir}'"
	else
		echo ">>> ERROR: ${swName} installation not found. Exiting..."
		return ${CRDNL_FAIL}
	fi
	return ${CRDNL_PASS}
}
findInstallDir "Python" "python_install_dirs.txt" "CRDNL_PYTHON_INSTALL_DIR" || return ${CRDNL_FAIL}
findInstallDir "CMake" "cmake_install_dirs.txt" "CRDNL_CMAKE_INSTALL_DIR" || return ${CRDNL_FAIL}
findInstallDir "GCC" "gcc_install_dirs.txt" "CRDNL_GCC_INSTALL_DIR" || return ${CRDNL_FAIL}
# Print variables ##################################################################################
echo ">>> Setting up environment variables"
echo "CRDNL_OS_LINUX .................... [${CRDNL_OS_LINUX}]"
echo "CRDNL_OS_WINDOWS .................. [${CRDNL_OS_WINDOWS}]"
echo "CRDNL_OS .......................... [${CRDNL_OS}]"
echo "CRDNL_PASS ........................ [${CRDNL_PASS}]"
echo "CRDNL_FAIL ........................ [${CRDNL_FAIL}]"
echo "CRDNL_TRUE ........................ [${CRDNL_TRUE}]"
echo "CRDNL_FALSE ....................... [${CRDNL_FALSE}]"
echo "CRDNL_ROOT_DIR .................... [${CRDNL_ROOT_DIR}]"
echo "CRDNL_BUILD_DIR ................... [${CRDNL_BUILD_DIR}]"
echo "CRDNL_BIN_DIR ..................... [${CRDNL_BIN_DIR}]"
echo "CRDNL_EXT_DIR ..................... [${CRDNL_EXT_DIR}]"
echo "CRDNL_BOOST_DIR ................... [${CRDNL_BOOST_DIR}]"
echo "CRDNL_EIGEN_DIR ................... [${CRDNL_EIGEN_DIR}]"
echo "CRDNL_PRJ_DIR ..................... [${CRDNL_PRJ_DIR}]"
echo "CRDNL_PYTHON_DIR .................. [${CRDNL_PYTHON_DIR}]"
echo "CRDNL_PYTHON_INSTALL_DIR .......... [${CRDNL_PYTHON_INSTALL_DIR}]"
echo "CRDNL_CMAKE_INSTALL_DIR ........... [${CRDNL_CMAKE_INSTALL_DIR}]"
echo "CRDNL_GCC_INSTALL_DIR ............. [${CRDNL_GCC_INSTALL_DIR}]"
return ${CRDNL_PASS}
