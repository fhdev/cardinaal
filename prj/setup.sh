#!/bin/bash
# Ensure script is sourced rather than run #########################################################
if [ "${BASH_SOURCE[0]}" -ef "$0" ]; then
    echo "Please source this script instead of running it!"
    return 1
fi
# Run bashrc #######################################################################################
if [ -f "${HOME}/.bashrc" ]; then
    . "${HOME}/.bashrc"
fi
# Import environment variables #####################################################################
SCRIPTROOT="$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &> /dev/null && pwd)"
. "${SCRIPTROOT}/env.sh" || return 1
# Add to path ######################################################################################
echo ">>> Adding software to path"
# Save old path 
OLD_PATH="${PATH}"
NEW_PATH="${OLD_PATH}"
# Remove directories from path if already there
CRDNL_PYTHON_SCRIPTS_DIR="${CRDNL_PYTHON_DIR}/Scripts"
NEW_PATH="$(echo ${NEW_PATH} | sed -e "s;:${CRDNL_GCC_INSTALL_DIR};;" -e "s;${CRDNL_GCC_INSTALL_DIR}:;;")"
NEW_PATH="$(echo ${NEW_PATH} | sed -e "s;:${CRDNL_CMAKE_INSTALL_DIR};;" -e "s;${CRDNL_CMAKE_INSTALL_DIR}:;;")"
NEW_PATH="$(echo ${NEW_PATH} | sed -e "s;:${CRDNL_PYTHON_SCRIPTS_DIR};;" -e "s;${CRDNL_PYTHON_SCRIPTS_DIR}:;;")"
NEW_PATH="$(echo ${NEW_PATH} | sed -e "s;:${CRDNL_PYTHON_DIR};;" -e "s;${CRDNL_PYTHON_DIR}:;;")"
# Add directories to beginning of path
NEW_PATH="${CRDNL_GCC_INSTALL_DIR}:${NEW_PATH}"
NEW_PATH="${CRDNL_CMAKE_INSTALL_DIR}:${NEW_PATH}"
NEW_PATH="${CRDNL_PYTHON_SCRIPTS_DIR}:${NEW_PATH}"
NEW_PATH="${CRDNL_PYTHON_DIR}:${NEW_PATH}"
export CRDNL_PATH="${NEW_PATH}"
export PATH="${NEW_PATH}"
# Jump to project root directory  ##################################################################
cd "${CRDNL_ROOT_DIR}"
return ${CRDNL_PASS}