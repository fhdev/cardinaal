# Ensure script is sourced rather than run #########################################################
if ($MyInvocation.InvocationName -ne '.' -and $MyInvocation.Line -ne '') {
    Write-Host "Please source this script instead of running it!"
    exit 1
}
# Obtain operating system ##########################################################################
${env:CRDNL_OS_LINUX} = "linux"
${env:CRDNL_OS_WINDOWS} = "windows"
if (${env:USERNAME}) {
    ${env:CRDNL_OS}=${env:CRDNL_OS_WINDOWS}
}
else {
    ${env:CRDNL_OS}=${env:CRDNL_OS_LINUX}
}
# Set up variables #################################################################################
${env:CRDNL_PASS} = 0
${env:CRDNL_FAIL} = 1
${env:CRDNL_TRUE} = 1
${env:CRDNL_FALSE} = 0
${env:CRDNL_ROOT_DIR} = [System.IO.Path]::GetDirectoryName($PSScriptRoot)
${env:CRDNL_BUILD_DIR} = [System.IO.Path]::Combine(${env:CRDNL_ROOT_DIR}, "build", ${env:CRDNL_OS})
${env:CRDNL_BIN_DIR} = [System.IO.Path]::Combine(${env:CRDNL_ROOT_DIR}, "bin", ${env:CRDNL_OS})
${env:CRDNL_EXT_DIR} = [System.IO.Path]::Combine(${env:CRDNL_ROOT_DIR}, "ext")
${env:CRDNL_BOOST_DIR} = [System.IO.Path]::Combine(${env:CRDNL_EXT_DIR}, "boost")
${env:CRDNL_EIGEN_DIR} = [System.IO.Path]::Combine(${env:CRDNL_EXT_DIR}, "eigen")
${env:CRDNL_PRJ_DIR} = [System.IO.Path]::Combine(${env:CRDNL_ROOT_DIR}, "prj")
${env:CRDNL_PYTHON_DIR} = [System.IO.Path]::Combine(${env:CRDNL_EXT_DIR}, "python", ${env:CRDNL_OS})
# Find tools #######################################################################################
function findInstallDir() {
    Param(
        ${swName},
        ${installDirsFile},
        ${envVarName}
    )
    ${dirFound} = $false
    ${installDir} = ""
    ${installDirs} = Get-Content -Path ([System.IO.Path]::Combine($PSScriptRoot, ${installDirsFile})) | Where-Object { $_ -notmatch "^#" }
    foreach(${installDir} in ${installDirs}) {
        if (Test-Path ${installDir}) {
            ${dirFound} = $true
            break
        }
    }
    if (${dirFound}) {
        Set-Item -Path "env:${envVarName}" -Value $installDir
        Write-Host ">>> ${swName} installation found at '${installDir}'"
        return ${env:CRDNL_PASS}
    }
    else {
        Write-Host ">>> ERROR: ${swName} installation not found. Exiting..."
        return ${env:CRDNL_FAIL}
    }
    
}
$success = findInstallDir "Python" "python_install_dirs.txt" "CRDNL_PYTHON_INSTALL_DIR"
if ($success -eq ${env:CRDNL_FAIL}) { exit ${env:CRDNL_FAIL} }
$success = findInstallDir "CMake" "cmake_install_dirs.txt" "CRDNL_CMAKE_INSTALL_DIR"
if ($success -eq ${env:CRDNL_FAIL}) { exit ${env:CRDNL_FAIL} }
$success = findInstallDir "GCC" "gcc_install_dirs.txt" "CRDNL_GCC_INSTALL_DIR"
if ($success -eq ${env:CRDNL_FAIL}) { exit ${env:CRDNL_FAIL} }
# Print variables ##################################################################################
Write-Host ">>> Setting up environment variables"
Write-Host "CRDNL_OS_LINUX .................... [${env:CRDNL_OS_LINUX}]"
Write-Host "CRDNL_OS_WINDOWS .................. [${env:CRDNL_OS_WINDOWS}]"
Write-Host "CRDNL_OS .......................... [${env:CRDNL_OS}]"
Write-Host "CRDNL_PASS ........................ [${env:CRDNL_PASS}]"
Write-Host "CRDNL_FAIL ........................ [${env:CRDNL_FAIL}]"
Write-Host "CRDNL_TRUE ........................ [${env:CRDNL_TRUE}]"
Write-Host "CRDNL_FALSE ....................... [${env:CRDNL_FALSE}]"
Write-Host "CRDNL_ROOT_DIR .................... [${env:CRDNL_ROOT_DIR}]"
Write-Host "CRDNL_BUILD_DIR ................... [${env:CRDNL_BUILD_DIR}]"
Write-Host "CRDNL_BIN_DIR ..................... [${env:CRDNL_BIN_DIR}]"
Write-Host "CRDNL_EXT_DIR ..................... [${env:CRDNL_EXT_DIR}]"
Write-Host "CRDNL_BOOST_DIR ................... [${env:CRDNL_BOOST_DIR}]"
Write-Host "CRDNL_EIGEN_DIR ................... [${env:CRDNL_EIGEN_DIR}]"
Write-Host "CRDNL_PRJ_DIR ..................... [${env:CRDNL_PRJ_DIR}]"
Write-Host "CRDNL_PYTHON_DIR .................. [${env:CRDNL_PYTHON_DIR}]"
Write-Host "CRDNL_PYTHON_INSTALL_DIR .......... [${env:CRDNL_PYTHON_INSTALL_DIR}]"
Write-Host "CRDNL_CMAKE_INSTALL_DIR ........... [${env:CRDNL_CMAKE_INSTALL_DIR}]"
Write-Host "CRDNL_GCC_INSTALL_DIR ............. [${env:CRDNL_GCC_INSTALL_DIR}]"
exit ${env:CRDNL_PASS}