# Ensure script is sourced rather than run #########################################################
if ($MyInvocation.InvocationName -ne '.' -and $MyInvocation.Line -ne '') {
    Write-Host "Please source this script instead of running it!"
    exit 1
}
# Run profile ######################################################################################
if (Test-Path "$Profile") {
    . "$Profile"
}
# Import environment variables #####################################################################
. "${PSScriptRoot}/env.ps1"
if ($LastExitCode -ne 0) { exit 1 }
# Add to path ######################################################################################
Write-Host ">>> Adding software to path"
# Save old path 
${OLD_PATH}=${env:PATH}
${NEW_PATH}=${OLD_PATH}
# Remove directories from path if already there
${CRDNL_PYTHON_SCRIPTS_DIR}="${env:CRDNL_PYTHON_DIR}\Scripts"
${NEW_PATH}=${NEW_PATH}.Replace(";${env:CRDNL_GCC_INSTALL_DIR}", "").Replace("${env:CRDNL_GCC_INSTALL_DIR};", "")
${NEW_PATH}=${NEW_PATH}.Replace(";${env:CRDNL_CMAKE_INSTALL_DIR}", "").Replace("${env:CRDNL_CMAKE_INSTALL_DIR};", "")
${NEW_PATH}=${NEW_PATH}.Replace(";${CRDNL_PYTHON_SCRIPTS_DIR}", "").Replace("${CRDNL_PYTHON_SCRIPTS_DIR};", "")
${NEW_PATH}=${NEW_PATH}.Replace(";${env:CRDNL_PYTHON_DIR}", "").Replace("${env:CRDNL_PYTHON_DIR};", "")
# Add directories to beginning of path
${NEW_PATH}="${env:CRDNL_GCC_INSTALL_DIR};${NEW_PATH}"
${NEW_PATH}="${env:CRDNL_CMAKE_INSTALL_DIR};${NEW_PATH}"
${NEW_PATH}="${CRDNL_PYTHON_SCRIPTS_DIR};${NEW_PATH}"
${NEW_PATH}="${env:CRDNL_PYTHON_DIR};${NEW_PATH}"
${env:CRDNL_PATH}=${NEW_PATH}
${env:PATH}=${NEW_PATH}
# Jump to project root directory  ##################################################################
Set-Location "${env:CRDNL_ROOT_DIR}"
exit ${env:CRDNL_PASS}